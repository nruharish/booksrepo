package org.nruharish;

import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.common.constraint.Assert;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import io.restassured.response.Response;

import javax.json.JsonArray;

@QuarkusTest
public class BookResourceTest {

    @Test
    public void shouldGetAllBooks(){
        given().when().get("/api/books")
        .then().statusCode(200).body("size()", is(3));
    }


    @Test
    public void testCountoFBooks(){
        given().when().get("/api/books/count").then().statusCode(200).body(is("3"));
    }
    
    @Test
    public void shouldGetABook(){
        given()
        .pathParam("id", 1)
        .when()
        .get("/api/books/{id}")
        .then()
        .statusCode(200)
        .body("title", is("Saswara Veda mantra"))
        .body("author", is("Ramakrishna mutt"))
        .body("yop", is(1970))
        .body("genre", is("Religion"));

    }
}