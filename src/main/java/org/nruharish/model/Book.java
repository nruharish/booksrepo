package org.nruharish.model;

public class Book {
    private int id;
    private String title;
    private String author;
    private int yop;
    private String genre;

    public Book(int id, String title, String author, int yop, String genre) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.yop = yop;
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYop() {
        return yop;
    }

    public void setYop(int yop) {
        this.yop = yop;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}