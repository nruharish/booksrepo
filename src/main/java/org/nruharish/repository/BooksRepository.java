package org.nruharish.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import org.nruharish.model.Book;


@ApplicationScoped
public class BooksRepository {

 
    private List<Book> books;



    public List<Book> getAllBooks(){
        List<Book> books = new ArrayList<>();
        //Book(int id, String title, String author, int yop, String genre)
        books.add(new Book(1, "Saswara Veda mantra", "Ramakrishna mutt", 1970, "Religion"));
        books.add(new Book(2, "Game of thrones", "Toklen", 2010, "Fantasy"));
        books.add(new Book(3, "Hyper focus", "ABC", 2015, "Self help"));

        return books;
    }


    public int getCountBooks(){
        return getAllBooks().size();
    }

    public Optional<Book> getBook(int id) {
        return getAllBooks().stream().filter( book -> book.getId() == id)
                .findFirst();
    }
}
