package org.nruharish;

import org.jboss.logging.Logger;
import org.nruharish.model.Book;
import org.nruharish.repository.BooksRepository;

import javax.inject.*;

import javax.annotation.Generated;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Path("/api/books")

public class BookResource {

    @Inject
    private BooksRepository bookRepository;

    @Inject
    private Logger logger;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Book> getAllBooks(){
        logger.info("get all books called");
        return bookRepository.getAllBooks();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/count")
    public int getCountBooks(){
        logger.info("count of books called");
        return bookRepository.getCountBooks();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Optional<Book> getBook(@PathParam("id") int id) {
        logger.info("get book called");
        return bookRepository.getBook(id);
    }
   
}
